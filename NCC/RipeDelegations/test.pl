# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..10\n"; }
END {print "not ok 1\n" unless $loaded;}
use NCC::RipeDelegations qw(@DELEGATIONS);
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

unless(@DELEGATIONS) {
    print "not ";
}
print "ok 2\n";

my $del = new NCC::RipeDelegations();

unless($del) {
    print "not ";
}
print "ok 3\n";

unless($del->isDelegation('217.0.0.0    -    217.255.255.255')) {
    print "not ";
}
print "ok 4\n";

unless($del->isDelegation('81.0.0.0    -    81.255.255.255')) {
    print "not ";
}
print "ok 5\n";

unless($del->Contains('81.100.2.3 - 81.100.22.33')) {
    print "not ";
}
print "ok 6\n";

unless($del->Contains('81.0.0.0 - 81.255.255.255')) {
    print "not ";
}
print "ok 7\n";

unless(!$del->Contains('181.100.2.3 - 181.100.22.33')) {
    print "not ";
}
print "ok 8\n";

unless($del->Contains('192.162.0.0 - 192.162.255.255')) {
    print "not ";
}
print "ok 9\n";

unless(!$del->Contains('192.168.0.0 - 192.168.255.255')) {
    print "not ";
}
print "ok 10\n";

