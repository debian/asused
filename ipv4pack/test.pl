# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..42\n"; }
END {print "not ok 1\n" unless $loaded;}
use ipv4pack;
$loaded = 1;
print "ok 1\n";

my $t = 1;

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

#
# test of routine quad2int
#

# print ("\n\nTesting quad2int\n",'=' x 16,"\n");
# known quad 1.1.1.1 is 16843009 (2**0+2**8+2**16+2**24)
# print ("\nvalid arguments: \n");
# print (" quad 1.1.1.1 int value should be 16843009: int = ",&quad2int('1.1.1.1'),"\n");

if (&quad2int('1.1.1.1') == 16843009) { $t++, print "ok 2\n" } else { print "not ok 2\n" }

if (&quad2int('1.400.0.0') == -1) { $t++, print "ok 3\n" } else { print "not ok 3\n" }
if (&quad2int('1.40.-1.0') == -1) { $t++, print "ok 4\n" } else { print "not ok 4\n" }
if (&quad2int('1.40.1')    == -1) { $t++, print "ok 5\n" } else { print "not ok 5\n" }


# non valid quads
# print ("\nnon-valid quads: \n");
# print (" quad 1.400.0.0: int =",&quad2int('1.400.0.0'),"\n");
# print (" quad 1.40.-1.0: int =",&quad2int('1.40.-1.0'),"\n");
# print (" quad 1.40.1   : int =",&quad2int('1.40.1'),"\n");

if (&int2quad(16843009) eq '1.1.1.1') { $t++, print "ok 6\n" } else { print "not ok 6\n" }

# print ("\nTesting int2quad\n",'=' x 16,"\n");
# print ("\nvalid arguments: \n");
# known quad 1.1.1.1 is 33686017
# print ("int 16843009 quad value 1.1.1.1 : quad = ",&int2quad(16843009),"\n");

if (&int2quad(9999999999) == -1) { $t++, print "ok 7\n" } else { print "not ok 7\n" }
if (&int2quad(-1) == -1) { $t++, print "ok 8\n" } else { print "not ok 8\n" }

# non valid ints
# print ("\nnon-valid ints: \n");
# print (" int 9999999999: quad =",&int2quad(1.400.0.0),"\n");
# print (" int -1: quad =",&int2quad(1.40.-1.0),"\n");

my ($range,$error) = &normalizerange('1 - 1.128');
if (($range eq '1.0.0.0 - 1.128.255.255') and ($error == 1)) 
	{ $t++, print "ok 9\n" } else { print "not ok 9\n"}

($range,$error) = &normalizerange('172.1 - 172.2');
if (($range eq '172.1.0.0 - 172.2.255.255') and ($error == 1)) 
	{ $t++, print "ok 10\n" } else { print "not ok 10\n"}

($range,$error) = &normalizerange('192.168 - 192.168.5');
if ($error == 23) 
	{ $t++, print "ok 11\n" } else { print "not ok 11\n"}

($range,$error) = &normalizerange('192.150 - 192.150.5');
if (($range eq '192.150.0.0 - 192.150.5.255') and ($error == 1)) 
	{ $t++, print "ok 12\n" } else { print "not ok 12\n"}

($range,$error) = &normalizerange('1/12');
if (($range eq '1.0.0.0 - 1.15.255.255') and ($error == 1)) 
	{ $t++, print "ok 13\n" } else { print "not ok 13\n"}

($range,$error) = &normalizerange('172.8/16');
if (($range eq '172.8.0.0 - 172.8.255.255') and ($error == 1)) 
	{ $t++, print "ok 14\n" } else { print "not ok 14\n"}

($range,$error) = &normalizerange('192.17/16');
if (($range eq '192.17.0.0 - 192.17.255.255') and ($error == 1)) 
	{ $t++, print "ok 15\n" } else { print "not ok 15\n"}

($range,$error) = &normalizerange('1');
if (($range eq '1.0.0.0 - 1.255.255.255') and ($error == 21)) 
	{ $t++, print "ok 16\n" } else { print "not ok 16\n"}


# print ("\nTesting normalizerange\n",'=' x 22,"\n");
# print ("\nvalid arguments: \n");
# known valid range quad - quad: 1.0.0.0 - 1.255.255.255 
#print (" quad range 1 - 1.128: range = ",join(' ERRORCODE: ',&normalizerange('1 - 1.128')),"\n");
#print (" quad range 172.1 - 172.2: range = ",join(' ERRORCODE: ',&normalizerange('172.1 - 172.2')),"\n");
#print (" quad range 192.168 - 192.168.5: range = ",join(' ERRORCODE: ',&normalizerange('192.168 - 192.168.5')),"\n");
#print (" quad range 192.150 - 192.150.5: range = ",join(' ERRORCODE: ',&normalizerange('192.150 - 192.150.5')),"\n");
# known valid range quad - quad: 1.0.0.0 - 1.128.255.255 
# print (" quad range 1/12: range = ",join(' ERRORCODE: ',&normalizerange('1/12')),"\n");
# print (" quad range 172.8/16: range = ",join(' ERRORCODE: ',&normalizerange('172.8/16')),"\n");
# print (" quad range 192.17/16: range = ",join(' ERRORCODE: ',&normalizerange('192.17/16')),"\n");

# known valid range quad: 1.0.0.0 - 1.255.255.255 
# print (" quad range 1: range = ",join(' ERRORCODE: ',&normalizerange('1')),"\n");

($range,$error) = &normalizerange('1.128 - 1.5');
if ($error == 26) 
	{ $t++, print "ok 17\n" } else { print "not ok 17\n"}

($range,$error) = &normalizerange('127.1 - 127.2');
if ($error == 23) 
	{ $t++, print "ok 18\n" } else { print "not ok 18\n"}

($range,$error) = &normalizerange('0 - 0.0.0.128');

if ($error == 22) 
	{ $t++, print "ok 19\n" } else { print "not ok 19\n"}

# non valid ints
# print ("\nnon-valid ranges: \n");

# quad - quad
# print (" quad range 1.128 - 1.5: range = ",join(' ERRORCODE: ',&normalizerange('1.128 - 1.5')),"\n");
# print (" quad range 127.1 - 127.2: range = ",join(' ERRORCODE: ',&normalizerange('127.1 - 127.2')),"\n");
# print (" quad range 223 - 230: range = ",join(' ERRORCODE: ',&normalizerange('127.1 - 127.2')),"\n");
# print (" quad range 0 - 0.128: range = ",join(' ERRORCODE: ',&normalizerange('0 - 0.128')),"\n");

($range,$error) = &normalizerange('1/128');
if ($error == 25) 
	{ $t++, print "ok 20\n" } else { print "not ok 20\n"}

($range,$error) = &normalizerange('1/-28');
if ($error == 27) 
	{ $t++, print "ok 21\n" } else { print "not ok 21\n"}

($range,$error) = &normalizerange('1.5/8');
if ($error == 25) 
	{ $t++, print "ok 22\n" } else { print "not ok 22\n"}

($range,$error) = &normalizerange('172.17/16');
if ($error == 23) 
	{ $t++, print "ok 23\n" } else { print "not ok 23\n"}

($range,$error) = &normalizerange('192.168/16');
if ($error == 23) 
	{ $t++, print "ok 24\n" } else { print "not ok 24\n"}

# quad/prefix
# print (" quad range 1/128: range = ",join(' ERRORCODE: ',&normalizerange('1/128')),"\n");
# print (" quad range 1/-28: range = ",join(' ERRORCODE: ',&normalizerange('1/-28')),"\n");
# print (" quad range 1.5/8: range = ",join(' ERRORCODE: ',&normalizerange('1.5/8')),"\n");
# print (" quad range 172.17/16: range = ",join(' ERRORCODE: ',&normalizerange('172.17/16')),"\n");
# print (" quad range 192.168/16: range = ",join(' ERRORCODE: ',&normalizerange('192.168/16')),"\n");

($range,$error) = &normalizerange('1/128');
if (error2str($error) eq  'Invalid index was given') 
	{ $t++, print "ok 25\n" } else { print "not ok 25\n"}

($range,$error) = &normalizerange('1/-28');
if (error2str($error) eq  'Syntax error was found') 
	{ $t++, print "ok 26\n" } else { print "not ok 26\n"}

($range,$error) = &normalizerange('1.5/8');
if (error2str($error) eq  'Invalid index was given') 
	{ $t++, print "ok 27\n" } else { print "not ok 27\n"}

($range,$error) = &normalizerange('172.17/16');
if (error2str($error) eq  'IP from private range was given') 
	{ $t++, print "ok 28\n" } else { print "not ok 28\n"}

($range,$error) = &normalizerange('192.168/16');
if (error2str($error) eq  'IP from private range was given') 
	{ $t++, print "ok 29\n" } else { print "not ok 29\n"}

# error2str
# print (" quad range 1/128: range = ",error2str((&normalizerange('1/128'))[1]),"\n");
# print (" quad range 1/-28: range = ",error2str((&normalizerange('1/-28'))[1]),"\n");
# print (" quad range 1.5/8: range = ",error2str((&normalizerange('1.5/8'))[1]),"\n");
# print (" quad range 172.17/16: range = ",error2str((&normalizerange('172.17/16'))[1]),"\n");
# print (" quad range 192.168/16: range = ",error2str((&normalizerange('192.168/16'))[1]),"\n");


($range,$error) = &normalizerange('192.168');
if ($error == 23) 
	{ $t++, print "ok 30\n" } else { print "not ok 30\n"}

($range,$error) = &normalizerange('172.20');
if ($error == 23) 
	{ $t++, print "ok 31\n" } else { print "not ok 31\n"}

($range,$error) = &normalizerange('10.10');
if ($error == 23) 
	{ $t++, print "ok 32\n" } else { print "not ok 32\n"}

($range,$error) = &normalizerange('10000000');
if ($error == 23) 
	{ $t++, print "ok 33\n" } else { print "not ok 33\n"}


# classful quad
# print (" classful quad range 192.168: range = ",join(' ERRORCODE: ',&normalizerange('192.168')),"\n");
# print (" classful quad range 172.20: range = ",join(' ERRORCODE: ',&normalizerange('172.20')),"\n");
# print (" classful quad range 10.10: range = ",join(' ERRORCODE: ',&normalizerange('10.10')),"\n");

# completely wrong
# print (" range in ints 99900000: range = ",join(' ERRORCODE: ',&normalizerange('10000000')),"\n");

# print ("\nTesting range2prefixes\n",'=' x 22,"\n");
# print ("\nvalid arguments: \n");
# known range int - int

if ([&range2prefixes('3232235520 - 3232301055')]->[0] eq '192.168.0.0/16') 
	{ $t++, print "ok 34\n" } else { print "not ok 34\n"}

# print (" int range 3232235520 - 3232301055; /16: prefix = ",&range2prefixes('3232235520 - 3232301055'),"\n");

my ($r1, $e1) = &normalizerange('1.1/16');
my ($r2, $e2) = &normalizerange('1.1.128/24');

if (&testoverlap($r1,$r2) == 110) 
	{ $t++, print "ok 35\n" } else { print "not ok 35\n"}

if (&testoverlap($r2,$r1) == 101) 
	{ $t++, print "ok 36\n" } else { print "not ok 36\n"}

($r1,$e1) = &normalizerange('1.2/16');

if (&testoverlap($r1,$r2) == -1) 
	{ $t++, print "ok 37\n" } else { print "not ok 37\n"}

if (&testoverlap('1.1.0.0 - 1.2.255.255','1.2.0.0 - 1.3.255.255') == 100) 
	{ $t++, print "ok 38\n" } else { print "not ok 38\n"}

if (&testoverlap('0','0') == 27) 
	{ $t++, print "ok 39\n" } else { print "not ok 39\n"}


# test testoverlap
# print ("\n\nTesting testoverlap\n",'=' x 19,"\n");
# print ("\nvalid arguments: \n");
# known quad 1.1/16 and 1.1.128/8
# my($range,$errorcode)=&normalizerange('1.1/16');
# my($range1,$errorcode1)=&normalizerange('1.1.128/24');
# print (" overlap value between 1.1/16 and 1.1.128/24: ",&testoverlap($range,$range1),"\n");
# print (" overlap value between 1.1.128/24 and 1.1/16: ",&testoverlap($range1,$range),"\n");
# ($range,$errorcode)=&normalizerange('1.2/16');
# print (" overlap value between 1.2/16 and 1.1.128/24: ",&testoverlap($range,$range1),"\n");

# print (" overlap value between 1.1.0.0 - 1.2.255.255 and 1.2.0.0 - 1.3.255.255: ",&testoverlap('1.1.0.0 - 1.2.255.255','1.2.0.0 - 1.3.255.255'),"\n");

# print ("\ninvalid arguments: \n");
# print (" overlap value between 0 and 0: ",&testoverlap('0','0'),"\n");

($r1,$e1) = &normalizerange('1.0/16');
($r2,$e2) = &normalizerange('1.1/16');

if (&aggregate($r1,$r2) eq '1.0.0.0/15') 
	{ $t++, print "ok 40\n" } else { print "not ok 40\n"}

($r1,$e1) = &normalizerange('1.2/16');

if (&aggregate($r2,$r1) == -1) 
	{ $t++, print "ok 41\n" } else { print "not ok 41\n"}

($range,$error)=&normalizerange121('195.19.209.22');
if (($error=='20') and ($range eq '195.19.209.22 - 195.19.209.22'))
	{  $t++, print "ok 42\n" } else { print "not ok 42\n"}


# test aggregate
# print ("\n\nTesting aggregate\n",'=' x 17,"\n");
# print ("\nvalid arguments: \n");
# ($range,$errorcode)=&normalizerange('1.0/16');
# $range1,$errorcode1)=&normalizerange('1.1/16');
# print (" aggregatable range between 1.0/16 and 1.1/16: ",&aggregate($range,$range1),"\n");

# print ("\ninvalid arguments: \n");
# ($range,$errorcode)=&normalizerange('1.2/16');
# print (" aggregatable range between 1.1/16 and 1.2/16: ",&aggregate($range1,$range),"\n");


# iprange being just a number
# print ("\n\nTesting iprange being just a number\n",'=' x 17,"\n");
# ($range,$errorcode)=&normalizerange121('195.19.209.22');
# print (" range of 195.19.209.22 : ",$range,"\n");

# print "\n";

if ($t == 42)
{
	print "\nAll tests successful.\n\n";
}
else
{
	print ("Failed ",(42 - $t)," tests.\n");
}


