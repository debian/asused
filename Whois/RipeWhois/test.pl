# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..4\n"; }
END { print "not ok 1\n" unless $loaded; }
use RipeWhois;
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

my $whois = new RipeWhois('KeepAlive' => 1, 'FormatMode' => 2, 'Debug' => 1);

if(ref($whois)) {
    print "ok 2\n";
}
else {
    print "not ok 2\n";
    exit;
}

if($whois->GetErrorCode()) {
    printf("Query error: %s\n", $whois->GetErrorString());
    print "not ok 3\n";
    exit;
}
else {
    print "ok 3\n";
}

my $test = 4;

foreach my $object (qw(BAT-RIPE TIB-RIPE CREW-RIPE)) {
    
    my @result = $whois->QueryObjects($object);
    
    unless(@result) {
	printf("Query error: %s\n", $whois->GetErrorString());
	print "not ok $test\n";
	exit;
    }
    else {
	print "ok $test\n";
	print "-" x 30, "\n";
	foreach (@result) {
	    print "$_\n";
	    print "-" x 30, "\n";
	}
    }
    $test++;
}
